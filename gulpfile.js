var gulp    = require( 'gulp' ),
    jshint  = require( 'gulp-jshint' ),
    concat  = require( 'gulp-concat' ),
    uglify  = require( 'gulp-uglify' ),
    notify  = require( 'gulp-notify' ),
    plumber = require( 'gulp-plumber' ),
    rename  = require( 'gulp-rename' ),
    sass    = require( 'gulp-ruby-sass' ),
    reload  = require( 'gulp-reload' ),
    bower   = require( 'main-bower-files' );

// turn the sources into arrays to order them.
// If they start with an exclamation mark they will be ignored
var paths = {
    html: {
        source: 'index.html',
    },
    sass: {
        source: 'style/sass/*.scss',
        destination: 'dist/style'
    },
    js: {
        source: [
            'scripts/game.js',
            'scripts/assetpositions/*.js',
            'scripts/components.js',
            'scripts/components/*.js',
            'scripts/helpers/storage.js',
            'scripts/helpers/sound.js',
            'scripts/helpers/dialogs.js',
            'scripts/scenes/*.js',
            'scripts/assets.js',
        ],
        destination: 'scripts/main.js'
    },
};

/**
 * Object which contains the custom notify messages
 *
 * @type {Object}
 */
var notifier = {
    /**
     * Function for showing an errormessage with Notify, triggered by Plumber
     *
     * @param  {Object} err This object contains the information of th error
     *
     * @return {void}
     */
    onError: function(err) {
        console.log( err );
        notify.onError( {
            title: 'Plugin: <%= error.plugin %>',
            message: 'Error: <%= error.message %>',
            sound: 'Beep'
        } )( err );
        this.emit( 'end' );
    },

    /**
     * Function for showing and 'restyling' the messages
     *
     * @param  {[type]} file [description]
     *
     * @return {void}
     */
    lintError: function(file) {
        // Don't show something if success
        if (file.jshint.success) {
            return false;
        }

        var errors = file.jshint.results.map( function(data) {
            if (data.error) {
                return '(' + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
            }
        } ).join( ' tt ' );

        return file.relative + ' (' + file.jshint.results.length + ' errors)\n' + errors;
    }

};

/**
 * Validate javascript.
 * If there are any errors a message is displayed.
 */
gulp.task( 'lint', function() {
    return gulp.src( paths.js.source )
        .pipe( jshint() )
        .pipe( notify( {
            title: 'Lint-error',
            message: function(file) {
                return notifier.lintError( file );
            }
        } ) );
} );

/**
 * Concat javascript and and uglify the
 */
gulp.task( 'scripts', function() {
    var headerValue = '// evaluated by Gulp.\n';
    return gulp.src( paths.js.source )
        /**
         * Call Plumber and give it the settings for errorhandling
         */
        .pipe( plumber( {
            errorHandler: notifier.onError
        } ) )
        .pipe( concat( 'main.js' ) )
        .pipe( gulp.dest( 'dist/scripts' ) )
        .pipe( rename( 'main.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( 'dist/scripts' ) )
        /**
         * Stop plumber so it won't work wthin the other tasks
         */
        .pipe( plumber.stop() );
    // .pipe( connect.reload() );
} );

/**
 * Move the index.html to the dist folder
 */
gulp.task( 'html', function() {
    return gulp.src( paths.html.source )
        /**
         * Call Plumber and give it the settings for errorhandling
         */
        .pipe( plumber( {
            errorHandler: notifier.onError
        } ) )
        .pipe( gulp.dest( 'dist' ) )
        /**
         * Stop plumber so it won't work wthin the other tasks
         */
        .pipe( plumber.stop() );
    // .pipe( connect.reload() );
} );


/**
 * Handle scss files for development purposes
 */
gulp.task( 'sass_dev', function() {

    return gulp.src( paths.sass.source )
        /**
         * Call Plumber and give it the settings for errorhandling
         */
        .pipe( plumber( {
            errorHandler: notifier.onError
        } ) )
        /**
         * Calling Sass with development settings
         */
        .pipe( sass( {
            style: 'expanded',
            debugInfo: true
        } ) )
        /**
         * Set and write to the desitnation folder
         */
        .pipe( gulp.dest( paths.sass.destination ) )
        /**
         * Stop plumber so it won't work wthin the other tasks
         */
        .pipe( plumber.stop() )
        /**
         * We're done, so reload the page/inject the style
         */
        .pipe( connect.reload() );
} );

gulp.task('bower', function() {
    return gulp.src(bower(/* options */), { base: 'bower_components' })
        .pipe( gulp.dest('scripts/libs') );
});

/**
 * List all the watch tasks per filetype
 */
gulp.task( 'watch', function() {
    gulp.watch( 'scripts/**/*.js', ['lint', 'scripts'] );
    gulp.watch( 'style/sass/*.scss', ['sass_dev'] );
    gulp.watch( '**/*.html', ['html'] )
} );

gulp.task( 'default', ['watch'] );
gulp.task( 'build', ['script', 'sass_dev', 'html'] );






















