var assets_level1 = [
	{ type: 'Border', x: 5, y: 14 },
	{ type: 'Border', x: 6, y: 14 },
	{ type: 'Border', x: 6, y: 13 },

	{ type: 'Dirt', x: 10, y: 2 },
	{ type: 'Dirt', x: 11, y: 2 },
	{ type: 'Dirt', x: 12, y: 2 },
	{ type: 'Dirt', x: 13, y: 2 },
	{ type: 'Dirt', x: 14, y: 2 },
	{ type: 'Dirt', x: 15, y: 2 },

	{ type: 'Dirt', x: 22, y: 2 },
	{ type: 'Dirt', x: 23, y: 2 },

	{ type: 'Dirt', x: 5, y: 4 },
	{ type: 'Dirt', x: 6, y: 3 },
	{ type: 'Dirt', x: 7, y: 3 },
	{ type: 'Dirt', x: 8, y: 3 },

	{ type: 'Dirt', x: 12, y: 4 },
	{ type: 'Dirt', x: 13, y: 4 },
	{ type: 'Dirt', x: 14, y: 4 },
	{ type: 'Dirt', x: 15, y: 4 },
	{ type: 'Dirt', x: 16, y: 4 },
	{ type: 'Dirt', x: 17, y: 4 },

	{ type: 'Dirt', x: 0, y: 6 },
	{ type: 'Dirt', x: 1, y: 6 },
	{ type: 'Dirt', x: 2, y: 6 },
	{ type: 'MovableBlock', x: 3, y: 6 },

	{ type: 'Dirt', x: 18, y: 7 },
	{ type: 'Dirt', x: 19, y: 7 },
	{ type: 'Dirt', x: 20, y: 7 },

	{ type: 'Dirt', x: 22, y: 7 },

	{ type: 'Dirt', x: 8, y: 8 },
	{ type: 'Dirt', x: 9, y: 8 },
	{ type: 'Dirt', x: 10, y: 8 },
	{ type: 'Dirt', x: 11, y: 8 },
	{ type: 'Dirt', x: 12, y: 8 },
	{ type: 'Dirt', x: 13, y: 8 },

	{ type: 'Dirt', x: 21, y: 8 },
	
	{ type: 'Dirt', x: 0, y: 10 },
	{ type: 'Dirt', x: 1, y: 10 },
	{ type: 'Dirt', x: 2, y: 10 },
	{ type: 'Dirt', x: 3, y: 10 },
	{ type: 'Dirt', x: 4, y: 11 },
	// { type: 'Dirt', x: 5, y: 10 },

	{ type: 'Dirt', x: 11, y: 10 },
	{ type: 'Dirt', x: 12, y: 10 },
	{ type: 'Dirt', x: 13, y: 10 },
	{ type: 'Dirt', x: 14, y: 10 },
	{ type: 'Dirt', x: 15, y: 10 },
	{ type: 'Dirt', x: 16, y: 10 },

	{ type: 'Chest', x: 12, y: 3 },
	{ type: 'Chest', x: 3, y: 9 },

	{ type: 'Rock', x: 11, y: 9 },
	{ type: 'Rock', x: 19, y: 13 },
	{ type: 'Rock', x: 19, y: 14 },
	{ type: 'Rock', x: 18, y: 14 },

	{ type: 'Marble', x: 1,  y: 9 },
	{ type: 'Marble', x: 14, y: 3 },
	{ type: 'Marble', x: 21, y: 7 },
	{ type: 'Marble', x: 13, y: 9 },
	{ type: 'Marble', x: 5, y: 3 },

	{ type: 'Key', x: 21, y: 14 },

	{ type: 'Bush', x: 14, y: 13 },

	{ type: 'Door', x: 23, y: 1 },

	// { type: 'Clock', x: 0, y: 0 },

];