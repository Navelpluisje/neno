var assets_level2 = [
	{ type: 'Dirt', x: 2, y:  2 },
	{ type: 'Dirt', x: 2, y:  5 },
	{ type: 'Dirt', x: 2, y:  8 },
	{ type: 'Dirt', x: 2, y: 11 },

	{ type: 'Dirt', x: 6, y:  1 },
	{ type: 'Dirt', x: 6, y:  4 },
	{ type: 'Dirt', x: 6, y:  7 },
	{ type: 'Dirt', x: 6, y: 10 },
	{ type: 'Dirt', x: 6, y: 13 },

	{ type: 'Dirt', x: 10, y:  3 },
	{ type: 'Dirt', x: 10, y:  6 },
	{ type: 'Dirt', x: 10, y:  9 },
	{ type: 'Dirt', x: 10, y: 12 },

	{ type: 'Dirt', x: 14, y:  1 },
	{ type: 'Dirt', x: 14, y:  4 },
	{ type: 'Dirt', x: 14, y:  7 },
	{ type: 'Dirt', x: 14, y: 10 },
	{ type: 'Dirt', x: 14, y: 13 },

	{ type: 'Dirt', x: 18, y:  2 },
	{ type: 'Dirt', x: 18, y:  5 },
	{ type: 'Dirt', x: 18, y:  8 },
	{ type: 'Dirt', x: 18, y: 11 },
	{ type: 'Dirt', x: 18, y: 14 },

	{ type: 'Dirt', x: 22, y:  3 },
	{ type: 'Dirt', x: 22, y:  6 },
	{ type: 'Dirt', x: 22, y:  9 },
	{ type: 'Dirt', x: 22, y: 12 },

	{ type: 'Dirt', x: 23, y:  3 },

	{ type: 'Water', x: 3, y: 15 },
	{ type: 'Water', x: 4, y: 15 },
	{ type: 'Water', x: 5, y: 15 },
	{ type: 'Water', x: 6, y: 15 },

	{ type: 'Water', x: 17, y: 15 },
	{ type: 'Water', x: 18, y: 15 },
	{ type: 'Water', x: 19, y: 15 },
	{ type: 'Water', x: 20, y: 15 },


	{ type: 'Marble', x: 2,  y:  7 },
	{ type: 'Marble', x: 6,  y:  3 },
	{ type: 'Marble', x: 10, y:  2 },
	{ type: 'Marble', x: 10, y: 11 },
	{ type: 'Marble', x: 14, y:  6 },
	{ type: 'Marble', x: 18, y: 10 },
	{ type: 'Marble', x: 22, y:  8 },

	{ type: 'Key_level2', x: 21, y: 14 },

	{ type: 'Door', x: 23, y: 2 },

];