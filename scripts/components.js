
// The Grid component allows an element to be located
//  on a grid of tiles
Crafty.c('Grid', {
	init: function() {
		this.attr({
			w: Game.map_grid.tile.width,
			h: Game.map_grid.tile.height
		});
	},

	// Locate this entity at the given position on the grid
	at: function(x, y) {
		if (x === undefined && y === undefined) {
			return { x: this.x/Game.map_grid.tile.width, y: this.y/Game.map_grid.tile.height };
		} else {
			this.attr({ x: x * Game.map_grid.tile.width, y: y * Game.map_grid.tile.height });
			return this;
		}
	}
});

// An "Actor" is an entity that is drawn in 2D on canvas
//  via our logical coordinate grid
Crafty.c('Actor', {
	init: function() {
		this.requires('2D, Canvas, Grid');
	},
});

// A Tree is just an Actor with a certain sprite
Crafty.c('Border', {
	init: function() {
		this.requires('Actor, Solid, spr_empty');
	},
});

// A Tree is just an Actor with a certain sprite
Crafty.c('Key', {
	_score: 150,
	init: function() {
		this.requires('Actor, Solid, spr_key');
	},

	getKey: function() {
		Sound.playEffect( 'key_bleep' );
		Game.grepped_key = true;
		Game.score += this._score;
		Crafty.trigger( 'SetScore', Game.score );
		this.destroy();
		Crafty.trigger( 'GreppedKey', this );
		Crafty.trigger( 'StartRopeTween', this );
		Crafty.e( 'Dirt' ).at( 20, 3 );
	},
});

// A Rock is just an Actor with a certain sprite
Crafty.c('Water', {
	init: function() {
    	this.requires('Actor, Solid, spr_water');
	},
});

// A Rock is just an Actor with a certain sprite
Crafty.c('Rock', {
	init: function() {
    	this.requires('Actor, Border, Solid, Gravity, spr_rock');
	},
});

// A Rock is just an Actor with a certain sprite
Crafty.c('Dirt', {
	init: function() {
    	this.requires('Actor, Border, Solid, Gravity, spr_dirt');
	},
});

// A Rock is just an Actor with a certain sprite
Crafty.c('MovableBlock', {
	_move_x: 2,
	_move_y: 1,
	init: function() {
    	this.requires('Dirt, Tween, spr_movable');
	},
});

// A Rock is just an Actor with a certain sprite
Crafty.c('Chest', {
	init: function() {
    	this.requires('Actor, Border, Solid, Gravity, spr_chest');
	},
});

// A Bush is just an Actor with a certain sprite
Crafty.c('Bush', {
	init: function() {
		this.requires('Actor, Border, Solid, spr_bush');
		this.attr({
			w: 2 * Game.map_grid.tile.width,
			h: 2 * Game.map_grid.tile.height
		});
	},
});

/**
 * This is the player-controlled character
 */
Crafty.c('PlayerCharacter', {
	xpos: 0,
	ypos: 0,
	dbw: false, // Dead by Water

	/**
	 * Boolean to avoid multiple calls to Die
	 */
	died: false,
	init: function() {
		this.requires('Actor, Twoway, Collision, spr_player, SpriteAnimation, Gravity')
			.twoway(3, 5.5)
			.stopOnSolids()
			.gravity('Border')
			.onHit( 'Key', this.getKey )
			.onHit( 'Marble', this.getMarble )
			.onHit( 'Door', this.enterDoor )
			.onHit( 'Bug', this.die )
			.onHit( 'RopeGirl', this.die )
			.onHit( 'MovableBlock', this.moveBlock )
			.onHit( 'Dirt', this.touchDirt )
			.onHit( 'Water', this.deadByWater )
			// These next lines define our four animations
			//  each call to .animate specifies:
			//  - the name of the animation
			//  - the x and y coordinates within the sprite
			//     map at which the animation set begins
			//  - the number of animation frames *in addition to* the first one
			.reel('PlayerMovingDown',  600, 0, 0, 3)
			.reel('PlayerMovingRight', 600, 0, 1, 3)
			.reel('PlayerMovingUp',    600, 0, 2, 3)
			.reel('PlayerMovingLeft',  600, 0, 3, 3);

		// Watch for a change of direction and switch animations accordingly
		var animation_speed = 4;
		this.bind( 'KeyDown', function( e ){
			if( e.key === Crafty.keys.UP_ARROW || e.key === Crafty.keys.W ) {
				Sound.playEffect( 'jump' );
			}
		} );
		this.bind('NewDirection', function(data) {
			if (data.x > 0) {
				this.animate('PlayerMovingRight', -1);
				Sound.playAudio( 'walking', -1 );
			} else if (data.x < 0) {
				this.animate('PlayerMovingLeft', -1);
				Sound.playAudio( 'walking', -1 );
			} else  {
				// No movement, show the front
				this.animate('PlayerMovingDown', -1);
				Sound.stopAudio('walking');
				this.pauseAnimation();
			}
		});
	},

	// Registers a stop-movement function to be called when
	//  this entity hits an entity with the "Solid" component
	stopOnSolids: function() {
		this.onHit('Solid', this.stopMovement);

		return this;
	},

	// Stops the movement
	stopMovement: function() {
		this._speed = 0;
		if (this._movement) {
			this.x -= this._movement.x;
			this.y -= this._movement.y;
		}
	},

	// Respond to this player visiting a village
	getMarble: function(data) {
		marble = data[0].obj;
		marble.visit( this );
	},

	// Respond to this player visiting a village
	getKey: function(data) {
		key = data[0].obj;
		key.getKey();
	},

	enterDoor: function( data ) {
		door = data[0].obj;
		door.enter();
	},

	touchDirt: function( data ) {
		if( Math.abs( data[0].obj.y - this._y ) < 29 ) {
			this.attr( { x: this._x, y: this._y+24 } );
		}
	},

	moveBlock: function( data ) {
		var block = data[0].obj;
		block.tween( { x: 5 * 33, y: 7 * 33 }, 1000 );
	},

	die: function( data ) {
		/**
		 * If not dead already run the die code
		 * Otherwise skip this. The if is needed because this
		 *  function will be triggered multiple times.
		 */
		if( ! this.died ) {
			/**
			 * Set the dead-flag
			 * Increase the lives and play our funeral song
			 */
			this.died = true;
			Game.lives -= 1;
			if( ! this.dbw ) {
				Sound.playEffect( 'die' );			
			} else {
				Sound.playEffect( 'watersplash' );
				this.dbw = false;
			}
	
			/**
			 * Add a Tween to let the player fall down out of our sight.
			 * The TweenEnd will start the next live, or show the gameover scene
			 */
			this.addComponent( 'Tween' )
				.tween( { rotation: 180, x: this.x + 90, y: 750, alpha: 0 }, 1000 )

				.bind( 'TweenEnd', function() {
					if( Game.lives === 0 ) {
						Crafty.scene( 'GameOver' );
					} else {
						Crafty.scene( Game.levels[ Game.level ] );
					}
				} );
		}

	},

	deadByWater: function() {
		this.dbw = true;
		this.die();
	}
});

// A marble is a tile on the grid that the PC must visit in order to win the game
Crafty.c('Marble', {
	_score: 100,
	init: function() {
		this.requires('Actor, spr_marble');
	},

	/**
	 * Handle the picking of a Marble
	 *
	 * @param  {Object} player The player to get the position from
	 *
	 * @return {void}
	 */
	visit: function( player ) {
		Sound.stopAudio( 'walking' );
		Game.score += this._score;
		Crafty.trigger( 'SetScore', Game.score );
		Game.addMarble( this.x, this.y );
		Game.setPlayerPosition( player.x, player.y );
		this.destroy();
		Sound.playEffect( 'key_bleep' );
	}
});

/**
 * Declare our killer bug. Shall we overwin
 */
Crafty.c( 'Bug', {
	_direction : 'left',
	_speed     : 1700,
	_step      : Game.map_grid.tile.width,
	_left      : 8,
	_right     : 13,
	init: function() {
		this.requires( 'Actor, Solid, Tween, Collision' )
			.attr( {w: 33, h: 22, x: this.getStep( this._left ), y: this.getStep( 7 )} )
			.addComponent( 'spr_rbug' )
			.tween({ x: this.getStep( this._right ), y: this.getStep( 7 ) }, this._speed );
		this.bind( 'TweenEnd', function() {
			/**
			 * Check the current direction.
			 * Switch the direction, set the tween again
			 * Set anaother spriteitem to make it look like our bug is turning 
			 */
			if( this._direction === 'left' ) {
				this._direction = 'right';
				this.tween( { x: this.getStep( this._left ), y: this.getStep( 7 ) }, this._speed );
				this.removeComponent( 'spr_rbug' );
				this.addComponent( 'spr_lbug' );

			} else {
				this._direction = 'left';
				this.tween( { x: this.getStep( this._right ), y: this.getStep( 7 ) }, this._speed );
				this.removeComponent( 'spr_lbug' );
				this.addComponent( 'spr_rbug' );
			}
		} );
	},

	/**
	 * Calculate the stepsize. Multiplies {steps} with the global tile width
	 *
	 * @param  {Number} steps Number of tiles
	 *
	 * @return {Number}       Stepsize in pixels
	 */
	getStep: function( steps ) {
		return steps * this._step;
	}
} );

/**
 * Declare our killer bug. Shall we overwin
 */
Crafty.c( 'RopeGirl', {
	_direction : 'up',
	_speed     : 1500,
	_step      : Game.map_grid.tile.width,
	_top       : -1,
	_left      : 19,
	_bottom    :  4,
	init: function() {
		this.requires( 'Actor, Solid, Tween, Collision' )
			.attr( {w: 33, h: 33, x: this.getStep( this._left ), y: this.getStep( this._top )} )
			.addComponent( 'spr_ropedwn' );
		if( Game.grepped_key ) {
			this.startTween();
		}

		/**
		 * This binding makes you able to start tthe tween from anuwhere in the game
		 */
		this.bind( 'StartRopeTween', function() {
			this.startTween();
		} );

		/**
		 * Binding the tweenend to switch directions 
		 */
		this.bind( 'TweenEnd', function() {
			/**
			 * Check the current direction.
			 * Switch the direction, set the tween again
			 * Set anaother spriteitem to make it look like our bug is turning 
			 */
			if( this._direction === 'up' ) {
				this._direction = 'down';
				this.tween( { x: this.getStep( this._left ), y: this.getStep( this._top ) }, this._speed );
				this.removeComponent( 'spr_ropedwn' );
				this.addComponent( 'spr_ropeup' );

			} else {
				this._direction = 'up';
				this.tween( { x: this.getStep( this._left), y: this.getStep( this._bottom ) }, this._speed );
				this.removeComponent( 'spr_ropeup' );
				this.addComponent( 'spr_ropedwn' );
			}
		} );
	},

	/**
	 * Just start the tween
	 */
	startTween: function() {
		this.tween({ x: this.getStep( this._left ), y: this.getStep( this._bottom ) }, this._speed );
	},

	/**
	 * Calculate the stepsize. Multiplies {steps} with the global tile width
	 *
	 * @param  {Number} steps Number of tiles
	 *
	 * @return {Number}       Stepsize in pixels
	 */
	getStep: function( steps ) {
		return steps * this._step;
	}
} );

// A Tree is just an Actor with a certain sprite
Crafty.c('Door', {
	init: function() {
		this.requires('Actor, spr_door');
	},

	enter: function() {
		if( Game.grepped_key && Game.no_marbles ) {
			Game.endDate = Date.now();
			Crafty.scene( 'Victory' );
		}
	}
});



