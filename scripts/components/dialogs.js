// Define component "HideShow" (functions to hide/show entity by setting z and alpha values)
Crafty.c(' HideShow', {
	hide : function() {
		this.attr({
			z : 0,
			alpha : 0.01
		});
	},

	show : function() {
		this.attr({
			z : 2,
			alpha : 1
		});
	}

});
