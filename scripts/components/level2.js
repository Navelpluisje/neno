
// A Tree is just an Actor with a certain sprite
Crafty.c('Key_level2', {
    _score: 150,
    init: function() {
        this.requires('Actor, Solid, Collision, spr_key')
            .collision()
            .onHit( 'PlayerCharacter', this.getKey );

    },

    getKey: function( data ) {
        Sound.playEffect( 'key_bleep' );
        Game.grepped_key = true;
        Game.score += this._score;
        Crafty.trigger( 'SetScore', Game.score );
        this.destroy();
        Crafty.trigger( 'GreppedKey', this );
    },
});