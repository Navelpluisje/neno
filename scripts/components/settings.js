/**
 * Savebuttons and it's corresponding actions
 */
Crafty.c( 'SaveButton', {
	init: function() {
		this.requires('2D, Canvas, Mouse, btn_save')
			.attr( { w:56, h: 22 });
		this.bind( 'MouseOver', function() {
			this.removeComponent( 'btn_save' )
				.addComponent( 'btn_save_act' );
			} );
		this.bind( 'MouseOut', function() {
			this.removeComponent( 'btn_save_act' )
				.addComponent( 'btn_save' );
			} );
		this.bind( 'Click', function() {
			Crafty.trigger( 'SaveSettings' );
			Crafty.scene( 'Start' );
			} );
			
	},
});
	