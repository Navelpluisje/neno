
// A Tree is just an Actor with a certain sprite
Crafty.c('Button', {
	init: function() {
		this.requires('2D, Canvas, Mouse, btn_start')
			.attr( { w:125, h: 35 });
		this.bind( 'MouseOver', function() {
			this.removeComponent( 'btn_start' )
				.addComponent( 'btn_start_act' );
			} );
		this.bind( 'MouseOut', function() {
			this.removeComponent( 'btn_start_act' )
				.addComponent( 'btn_start' );
			} );
		this.bind( 'Click', function() {
			Game.setStartDate();
			Crafty.scene( Game.levels[ Game.level ] );
			} );
			
	},
});

// A Tree is just an Actor with a certain sprite
Crafty.c('SettingsButton', {
	init: function() {
		this.requires('2D, Canvas, Mouse, btn_settings')
			.attr( { w:181, h: 35 });
		this.bind( 'MouseOver', function() {
			this.removeComponent( 'btn_settings' )
				.addComponent( 'btn_settings_act' );
			} );
		this.bind( 'MouseOut', function() {
			this.removeComponent( 'btn_settings_act' )
				.addComponent( 'btn_settings' );
			} );
		this.bind( 'Click', function() {
			Crafty.scene( 'Settings' );
			} );
			
	},
});
