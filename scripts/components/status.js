/**
 * Define some components for time, nb of lives, score etc.
 */

/** 
 * We will show the score text
 */
Crafty.c( 'Score', {
	init: function() {
		this.requires( '2D, DOM, spr_score' );
		this.attr({x: 10, y: 37, w:71, h: 22});
	},
} );

/** 
 * We will show the time text
 */
Crafty.c( 'Time', {
	init: function() {
		this.requires( '2D, DOM, spr_time' );
		this.attr( { x: 10, y: 10, w:56, h: 22 } );
	},
} );

/** 
 * A component for creating the numbers used by the time
 */
Crafty.c( 'Number', {
	init: function() {
		this.requires( '2D, DOM, spr_nb0' );
		this.attr( { w:15, h: 22 } );
	},
} );

/** 
 * A component for creating the numbers used by the time
 */
Crafty.c( 'Colon', {
	init: function() {
		this.requires( '2D, DOM, spr_colon' );
		this.attr( { w:10, h: 22 } );
	},
} );

/** 
 * We will show the lives available
 */
Crafty.c( 'Lives', {
	init: function() {
		var key = '';
		if( Game.grepped_key ) {
			key = 'k';
		}
		this.requires( '2D, DOM, spr_live' + Game.lives + key );
		this.attr({x: 10, y: 64, w:80});
		this.bind( 'GreppedKey', function() {
			this.addComponent( 'spr_live' + Game.lives + 'k' );
		} );
	},
} );

/** 
 * We will show a timer to tell the time played
 */
Crafty.c( 'Clock', {
	_start_x: 85,
	_sprites: {
		sec1: '0',
		sec2: '0',
		min1: '0',
		min2: '0',
	},
	_init: true,
	init: function() {
		this.requires( '2D, DOM' );
		this.bind( 'EnterFrame', function( data ) {
			this.setTime( data );
   		} );
	},

	setTime: function( data ) {
		/**
		 * If this is the first time EnterFrame gets triggered we create all the elements
		 */
		if( this._init ) {
			this._init = false;
			/**
			 * Reset the sprites. Needed to start withe the right time after we've died
			 * If we don't do this the minutes and 10 seconds get refreshed to late.
			 */
			this._sprites= {
				sec1: '0',
				sec2: '0',
				min1: '0',
				min2: '0',
			};
			min1  = Crafty.e( 'Number' ).attr( { x:  90, y:10, w:15, h:22 } );
			min2  = Crafty.e( 'Number' ).attr( { x: 105, y:10, w:15, h:22 } );
			colon = Crafty.e( 'Colon'  ).attr( { x: 120, y:10, w:10, h:22 } );
			sec1  = Crafty.e( 'Number' ).attr( { x: 130, y:10, w:15, h:22 } );
			sec2  = Crafty.e( 'Number' ).attr( { x: 145, y:10, w:15, h:22 } );
		}

		/**
		 * Check on a change every fifth frame. Then:
		 * Calculate time difference en set the seconds and minutes.
		 * If any of these are changed we will chande the sprite 
		 */
		if( data.frame % 5 === 0 ) {
			var time = parseInt( ( data.gameTime - Game.startDate ) / 1000),
				secs = ( time % 60 < 10 ? '0' + time % 60 : time % 60 ).toString(),
				mins = ( parseInt(time / 6) < 10 ? '0' + parseInt(time / 60) : parseInt(time / 60) ).toString();

			if( this._sprites.min1 !== mins[0] ) {
				min1.removeComponent( 'spr_nb' + this._sprites.min1 );
				min1.addComponent( 'spr_nb' + mins[0] );
				this._sprites.min1 = mins[0];
			}

			if( this._sprites.min2 !== mins[1] ) {
				min2.removeComponent( 'spr_nb' + this._sprites.min2 );
				min2.addComponent( 'spr_nb' + mins[1] );
				this._sprites.min2 = mins[1];
			}

			if( this._sprites.sec1 !== secs[0] ) {
				sec1.removeComponent( 'spr_nb' + this._sprites.sec1 );
				sec1.addComponent( 'spr_nb' + secs[0] );
				this._sprites.sec1 = secs[0];
			}

			if( this._sprites.sec2 !== secs[1] ) {
				sec2.removeComponent( 'spr_nb' + this._sprites.sec2 );
				sec2.addComponent( 'spr_nb' + secs[1] );
				this._sprites.sec2 = secs[1];
			}
		}

	}
} );

/**
 * Show the correct score on the screen
 */
Crafty.c( 'ScoreBoard', {
	_init: true,
	_ScoreEnteties: [],
	_score: [],
	_xpos: 90,
	_ypos: 37,

	init: function() {
		this.requires( '2D, DOM' );
		this.bind( 'SetScore', function( score ) {
			this.setScore( score );
		} );
	},

	setScore: function( score ) {
		score = score.toString();

		/**
		 * On init we remove any existing entity. 
		 * This way there will be ni conflicts
		 */
		if( this._init ) {
			this._init = false;
			this._ScoreEnteties.forEach( function( entity, index ) {
				entity.destroy();
			}, this );
			/**
			 * All entities are destroyed, now we empty the arrays
			 */
			this._ScoreEnteties = [];
			this._score         = [];
		}

		/**
		 * Add an entity for any mnumber in the score
		 */
		if( score.length > this._ScoreEnteties.length ) {
			while( score.length > this._ScoreEnteties.length ) {
				this._addScore();
			}
		} else {
			while( score.length < this._ScoreEnteties.length ) {
				this._removeScore();
			}
		}

		/**
		 * After all the enteties have been added, 
		 * remove the 'old' sprite icon and set the new one
		 */
		for( var index in this._ScoreEnteties ) {
			this._ScoreEnteties[index].removeComponent( 'spr_nb' + this._score[ index ] );
			this._ScoreEnteties[index].addComponent( 'spr_nb' + score[ index ] );
			this._score[ index ] = score[ index ];
		}

	},

	_addScore: function( number ) {
		xpos = this._xpos + (this._ScoreEnteties.length) * 15;
		this._ScoreEnteties.push( Crafty.e( 'Number' ).attr( { x: xpos , y:this._ypos, w:15, h:22 } ) );
		if( typeof number === 'undefined' ) {
			this._score.push( '0' );
		} else {
			this._score.push( number );
		}
	},

	_removeScore: function() {
		this._score.pop();
		this._ScoreEnteties[ this._ScoreEnteties.length - 1 ].destroy();
		this._ScoreEnteties.pop();
	}

} );

