Crafty.c( 'Woohoo', {
	init: function() {
		this.requires( '2D, DOM, Image' )
			.image( '/assets/woohoo.png' )
			.attr( { x: Game.width()/2 - 33, y: 33 } );
	} 
} );

Crafty.c( 'LevelScore', {
	_init: true,
	_bonusinit: true,
	_BonusEnteties: [],
	_total_time: 0,
	_score: [],
	_bonus: 0,
	_xpos: 145,
	_ypos: 350,

	init: function() {
		this.requires( '2D, DOM' );
		this.bind( 'EnterFrame', function( data ) {
			if( this._init ) {
				this._total_time = 60 - parseInt( ( Game.endDate - Game.startDate ) / 1000 );
				// this._score = Game.score;
				this._init = false;
			}
			if( data.frame % 10 === 0 && this._total_time > 0 ) {
				this._bonus += 10;
				this._total_time --;
				this.setBonus();
			} 

			if( this._total_time <= 0  ) {
				Game.score += this._bonus;
				this.unbind( 'EnterFrame' );
				this._reset();
				Crafty.trigger( 'PutLevelScore' );
			}
		} );
	},

	setBonus: function( ) {
		var the_bonus = this._bonus.toString();

		Crafty.audio.play('ting');

		/**
		 * On init we remove any existing entity. 
		 * This way there will be ni conflicts
		 */
		if( this._bonusinit ) {
			this._bonusinit = false;
			this._BonusEnteties.forEach( function( entity, index ) {
				entity.destroy();
			}, this );
			/**
			 * All entities are destroyed, now we empty the arrays
			 */
			this._BonusEnteties = [];
			this._score         = [];
		}

		if( this._BonusEnteties.length === 0 ) {
			Crafty.e( 'Bonusimage' )
				.attr( {x: 45, y: this._ypos } );

			Crafty.e( 'Colon' )
				.attr( {x: 120, y: this._ypos } );

		}

		/**
		 * Add an entity for any mnumber in the score
		 */
		if( the_bonus.length > this._BonusEnteties.length ) {
			while( the_bonus.length > this._BonusEnteties.length ) {
				this._addNumber();
			}
		} else {
			while( the_bonus.length < this._BonusEnteties.length ) {
				this._removeNumber();
			}
		}

		/**
		 * After all the enteties have been added, 
		 * remove the 'old' sprite icon and set the new one
		 */
		for( var index in this._BonusEnteties ) {
			this._BonusEnteties[index].removeComponent( 'spr_nb' + this._score[ index ] );
			this._BonusEnteties[index].addComponent( 'spr_nb' + the_bonus[ index ] );
			this._score[ index ] = the_bonus[ index ];
		}
	},

	_addNumber: function( number ) {
		var xpos = this._xpos + (this._BonusEnteties.length) * 15;
		this._BonusEnteties.push( Crafty.e( 'Number' ).attr( { x: xpos , y:this._ypos, w:15, h:22 } ) );
		if( typeof number === 'undefined' ) {
			this._score.push( '0' );
		} else {
			this._score.push( number );
		}
	},

	_removeNumber: function() {
		this._score.pop();
		this._BonusEnteties[ this._BonusEnteties.length - 1 ].destroy();
		this._BonusEnteties.pop();
	},

	_reset: function() {
		this._init          = true;
		this._bonusinit     = true;
		this._BonusEnteties = [];
		this._total_time    = 0;
		this._score         = [];
		this._bonus         = 0;
		this._xpos          = 145;
		this._ypos          = 350;
	}

} );

// A Tree is just an Actor with a certain sprite
Crafty.c('Bonusimage', {
	init: function() {
		this.requires('2D, DOM, spr_bonus')
			.attr( { w:72, h:22 } );
	},
});

