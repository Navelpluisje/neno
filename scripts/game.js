Game = {
    /**
     * This defines our grid's size and the size of each of its tiles
     */
    map_grid: {
        width: 24,
        height: 16,
        tile: {
            width: 33,
            height: 33
        }
    },

    score: 0,

    /**
     * Start time of the game
     * @type {Number}
     */
    startDate: 0,

    /**
     * End time of the game
     * @type {Number}
     */
    endDate: 0,

    /**
     * Playerpostion vor the player on start.
     * Will be changed if th eplayer gets a marble
     * @type {Object}
     */
    playerpos: {
        x: 1 * 33,
        y: 13 * 33
    },

    /**
     * Number of live left. This will increase if the player dies
     * @type {Number}
     */
    lives: 3,

    /**
     * List of marbles already collected by the player
     * @type {Array}
     */
    marbles: [],
    no_marbles: false,

    /**
     * Did the player get the key??
     * @type {Boolean}
     */
    grepped_key: false,

    level: 1,

    levels: {
        1: 'Level1',
        2: 'Level2',
    },

    /**
     * The total width of the game screen. Since our grid takes up the entire screen
 	 *  this is just the width of a tile times the width of the grid
     */
    width: function() {
        return this.map_grid.width * this.map_grid.tile.width;
    },

    /**
     * The total height of the game screen. Since our grid takes up the entire screen
     *  this is just the height of a tile times the height of the grid
     */
    height: function() {
        return this.map_grid.height * this.map_grid.tile.height;
    },

    /**
     * Initialize and start our game
     */
    start: function() {
        // Start crafty and set a background color so that we can see it's working
        Crafty.init( Game.width(), Game.height() );
        Crafty.background( 'url(/assets/background.png)' );

        // Simply start the "Loading" scene to get things going
        Crafty.scene( 'Loading' );
    },

    /**
     * Reset all parameters on the start of a new game
     * 
     * @return {void} 
     */
    reset: function() {
        this.lives = 3;
        this.startDate = new Date();
        this.playerpos.x = 1 * 33;
        this.playerpos.y = 13 * 33;
        this.marbles = [];
        this.grepped_key = false;
        this.score = 0;
        this.level = 1;
    },

    setLevel: function() {
        this.marbles = [];
        this.no_marbles = false;
        this.grepped_key = false;
    },

    /**
     * Reset the player position. Will be used for repositioning the player after he dies
     * 
     * @return {void}
     */
    resetPlayerPosition: function() {
        this.setPlayerPosition( 1 * 33, 13 * 33 );
    },

    /** 
     * Add a marble to the list of collected Marbles
     * 
     * @param {Number} xpos horizontal position of the collected Marble
     * @param {Number} ypos vertical position of the collected Marble
     */
    addMarble: function(xpos, ypos) {
        this.marbles[xpos] = ypos;
        if( Crafty( 'Marble' ).length < 2) {
            this.no_marbles = true;
        }
    },

    setStartDate: function() {
        Game.startDate = Date.now();
    },

    /**
     * Set the player position.
     * Will be used to set the postion after the player collected a Marble and 
     * comes back to the gane cene
     *
     * @param {Number} xpos horizontal position of the collected Marble
     * @param {Number} ypos vertical position of the collected Marble
     */
    setPlayerPosition: function(xpos, ypos) {
        this.playerpos.x = xpos;
        this.playerpos.y = ypos;
    },

    /**
     * Check if a level really exists
     *
     * @param  {Number} level Number of the level you want to know the existance of
     *
     * @return {Boolean}      True if the level exists, false otherwise
     */
    checkLevel: function( level ) {
        if( Object.keys( this.levels ).indexOf( level.toString() ) === -1 ) {
            return false;
        }
        return true;
    },

};

$text_css = {
    'size': '24px',
    'family': 'Arial',
    'color': 'red',
    'text-align': 'center'
};
