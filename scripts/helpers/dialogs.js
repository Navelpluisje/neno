var exitGameScreen = function() {

    return {
        dimRectangle: null,
        resumeButton: null,
        quitButton: null,
        resumeText: null,
        quitText: null,
        questionMark: null,

        initialize: function() {
            var that = this;

            // DIM RECTANGLE
            this.dimRectangle = Crafty.e( '2D, DOM, Color, HideShow' )
                .attr( {
                    w: Game.width,
                    h: Game.height,
                    z: 0,
                    alpha: 0.001
                } )
                .color( '#000000' );

            // RESUME BUTTON
            this.resumeButton = Crafty.e( '2D, DOM, startBtn, HideShow' )
                .attr( {
                    x: 455,
                    y: 225,
                    z: 0,
                    alpha: 0.01
                } );
            this.resumeText = Crafty.e( '2D, DOM, resumeText, HideShow, Mouse' )
                .attr( {
                    x: 455,
                    y: 225,
                    z: 0,
                    alpha: 0.01
                } );

            // quit button and question mark declarations (omitted in this code
            // sample)
        },

        // exit the scene screen
        show: function() {
            this.dimRectangle.attr( {
                z: 1,
                alpha: 0.8
            } );
            this.resumeButton.show();
            this.resumeText.show();
            // this.quitText.show();
            // this.questionMark.show();

            // this.resumeText.bind('Click', function() {
            //     that.cancelExit();
            // });

            // this.quitText.bind('Click', function() {
            //     // quit the game
            // });
        },

        // return to the game (from exit screen)
        cancelExit: function() {
            this.dimRectangle.hide();
            this.resumeButton.hide();
            // this.quitButton.hide();
            this.resumeText.hide();
            // this.quitText.hide();
            // this.questionMark.hide();

            // this.resumeText.unbind('Click');
            // this.quitText.unbind('Click');
        },
    };
}();
