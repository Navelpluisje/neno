/**
 * A helper to manage the sounds and it's settings. This will make it easier to manage settings of sound.
 */

var Sound = {
	settings: {
		audio: 1,
		audio_vol: 0.3, 
		effects: 1,
		effects_vol: 0.3, 
	},

	init: function() {
		Crafty.audio.setChannels( 4 );
		this.setValue( 'audio', Storage.getItem( 'audio' ) === null ? 1 : parseInt( Storage.getItem( 'audio' ) ) );
		this.setValue( 'audio_vol', Storage.getItem( 'audio_vol' ) === null ? 0.3 : parseFloat( Storage.getItem( 'audio_vol' ) ) );
		this.setValue( 'effects', Storage.getItem( 'effects' ) === null ? 1 : parseInt( Storage.getItem( 'effects' ) ) );
		this.setValue( 'effects_vol', Storage.getItem( 'effects_vol' ) === null ? 0.3 : parseFloat( Storage.getItem( 'effects_vol' ) ) );
	},

	/**
	 * playAudio will play a sound within an endless loop. 
	 * Volume and mute is set witin the settings
	 *
	 * @param  {String} name Name of the file to play
	 *
	 * @return {void}
	 */
	playAudio: function( name ) {
		if( this.settings.audio === 1 ) {
			Crafty.audio.play( name, -1, this.settings.audio_vol );
		}
	},

	/**
	 * playEffect will play a sound only once. 
	 * Volume and mute is set witin the settings
	 *
	 * @param  {String} name Name of the file to play
	 *
	 * @return {void}
	 */
	playEffect: function( name ) {
		if( this.settings.effects === 1 ) {
			Crafty.audio.play( name, 1, this.settings.effects_vol );
		}
	},

	/**
	 * stopAudio will stop the sound with the given name,
	 * Or if no name has been set, all sounds will stop
	 *
	 * @param  {String} name Name of the file to stop
	 *
	 * @return {void}
	 */
	stopAudio: function( name ) {
		if( typeof name === 'undefined' ) {
			Crafty.audio.stop();			
		} else {
			Crafty.audio.stop( name );			
		}
	},

	stopEffect: function( name ) {
		this.stopAudio( name );
	},

	/**
	 * Set the value of a setting. Given in a key, value pair.
	 * If the key doesn;t exist, nothing happpens.
	 *
	 * @param {String} field Key of the value to set
	 * @param {Any} value Value to set
	 */
	setValue: function( field, value ) {
		if( this.settings.hasOwnProperty( field ) ) {
			this.settings[ field ] = value;
			Storage.setItem( field, value );
		}
	},

	/**
	 * Get a value of the settings by it's key.
	 * The correspoding value will be returned if the key exists. 
	 * An empty string otherwise.
	 *
	 * @param  {String} field The key to get the value of
	 *
	 * @return {Any}
	 */
	getValue: function( field ) {
		if( this.settings.hasOwnProperty( field ) ) {
			return this.settings[ field ];
		}
		return '';
	}
};

Sound.init();