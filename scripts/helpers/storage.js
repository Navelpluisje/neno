/**
 * A small helper for storing data in the browser
 */

var Storage = {
	available: true,

	init: function() {
		if (!window.localStorage) {
			Storage.available = false;
		}
	},

	setItem: function( key, value ) {
		localStorage.setItem( key, value );
	},

	getItem: function( key ) {
		return localStorage.getItem( key );
	},

	removeItem: function( key ) {
		localStorage.removeItem( key );
	},
};

Storage.init();