window.onload = function() {
	var ValidWords;
	var ResultsTags;
	var ScoreVal6=[1,3,6,25]; // scoring for six letter words
	var ScoreVal7=[1,3,6,10,25];  // scoring for seven letter words
	var ScoredWords = Array();
	var Score = 0;
	var TileSize = 64; // for spacing the tiles
	var WhichRound = 0; // which round we're on

	// -- GENERAL EXTERNAL INFO
	// set either by cookies (if we're the contest originator or playing single-player) or through GET form info (if challenged) 
	var Rounds = 1; // number of rounds
	var RoundTime = 120; // seconds for round time
	var GameType; // what kind of game - can be "single", "mychallenge", "challenged"
	
	// -- CONTEST EXTERNAL INFO --
	// set either by cookies (if we're the contest originator or playing single-player) or through GET form info (if challenged) 
	var MyContestCode = Array(); // strings for which words we got right
	var OpponentContestCode = Array(); // strings for which words they got right
	var MyRoundScore = Array(); // scores for individual rounds
	var OpponentRoundScore = Array(); // scores for individual rounds
	var GameNumbers = Array(); // game numbers
	var Clues = Array(); // list of letters/answers
	var MyTotalScore = 0;	// total challenge score
	var OpponentTotalScore = 0;
	var MyName;
	var MyEmail;
	var OpponentName;
	var OpponentEmail;
	
	// -- COOKIES ----------------------------------
	// cookie functions -- borrowed from http://www.w3schools.com/js/js_cookies.asp
	function setCookie(c_name,value,exdays)	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}

	function getCookie(c_name) {
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name) {
				return unescape(y);
			}
		}
	}
	// this one's mine, added after I read more on javascript cookies
	function deleteCookie(c_name)	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() - 365); // set expiration to a year ago; this will trigger browser to get rid of it
		var c_value="null; expires="+exdate.toUTCString();
		document.cookie=c_name + "=" + c_value;
	}

    // -- INITIALIZIATION 
	// Start crafty and set up canvas

    Crafty.init(830, 450);
	Crafty.canvas.init();

	
	// -- BASIC PARAMETERS
	// see if cookies exist for rounds, roundtime, gametype; 
	// defaults are already set for 1 round, 120 sec, gametype should default to single

	// rounds
	var tmp;
	tmp = parseInt(getCookie("rounds"));
	if (tmp>0 && tmp < 11) Rounds = tmp;
	
	// roundtime
	tmp = parseInt(getCookie("roundtime"));
	if (tmp == 30 || 
		tmp == 60 ||
		tmp == 120 ||
		tmp == 180 ) RoundTime = tmp;

	// game type
	GameType = getCookie("gametype");
	if (GameType == undefined) GameType = "single";

	// -- CHALLENGES
	// Option 1. We've been challenged

	if (GameType == "challenged") {
		MyName = getCookie("player2");
		MyEmail = getCookie("email2");
		OpponentName = getCookie("player1");
		OpponentEmail = getCookie("email1");
		MyTotalScore = 0;
		OpponentTotalScore = parseInt(getCookie("p1totalscore"));
		SeqNum = getCookie("seqnum");
		for (var i=0;i<Rounds;i++) {
			GameNumbers[i] = parseInt(getCookie("gamenum"+i));
			Clues[i] = getCookie("clue"+i).replace(/[+]/g," "); 
			OpponentRoundScore[i] = getCookie("p1score"+i);
			OpponentContestCode[i] = getCookie("p1code"+i);
			MyRoundScore[i] = 0;
			MyContestCode[i] = "";
		}
	}
	
	// Option 2. We're challenging somebody
	else if (GameType == "mychallenge") {
		MyName = getCookie("player1");
		MyEmail = getCookie("email1");
		OpponentName = getCookie("player2");
		OpponentEmail = getCookie("email2");
		MyTotalScore = 0;
		OpponentTotalScore = 0;
		SeqNum = getCookie("seqnum");
		for (var i=0;i<Rounds;i++) {
			GameNumbers[i] = parseInt(getCookie("gamenum"+i));
			Clues[i] = getCookie("clue"+i).replace(/[+]/g," "); 
			OpponentRoundScore[i] = 0;
			OpponentContestCode[i] = "";
			MyRoundScore[i] = 0;
			MyContestCode[i] = "";
		}
	}

	// Option 3. Single player game
	else { // gametype should be "single" but we'll make this the catch-all in case that's unset or set wrong
		GameType = "single"
		MyName = "";
		MyEmail = "";
		OpponentName = "";
		OpponentEmail = "";
		MyTotalScore = 0;
		OpponentTotalScore = 0;
		for (var i=0;i<Rounds;i++) {
			GameNumbers[i] = getCookie("gamenum"+i);
			Clues[i] = getCookie("clue"+i).replace(/[+]/g," "); 
			OpponentRoundScore[i] = 0;
			OpponentContestCode[i] = "";
			MyRoundScore[i] = 0;
			MyContestCode[i] = "";
		}
	}
	
	// -- COMPONENTS -------------------------------
	// These are Crafty components that add functionality to Crafty entities
	
	// letter tile component
	Crafty.c('lettertile', {
		// public
		tileno: 0,
		position: 0,
		letter: '-',
		typed: false,
		
		restore: function() {
			this.x = this.rackleft+this.position*TileSize;
			this.y = 88;
			this.typed = false;
		},
			
	});

	// message tile component
	Crafty.c('messageboard', {
		// public
		timer: 0,
		alpha: 0,
		
		init: function() {
			this.bind("NewMessage",this.updatemessage);
			this.bind("EnterFrame",this.handle);
		},
		handle: function() {
			this.timer--;
			if (this.timer < 0) this.alpha = 0;
			else if (this.timer > 100) this.alpha = 1;
			else this.alpha = this.timer/100;
		},
		updatemessage: function(name) {
			this.addComponent(name);
			this.timer = 200;
		}
			
	});

	
	// Keyboard handler component - handles keyboard input and runs the game
	Crafty.c('keyboardhandler', {
		
		// public
		entry: "",
		entrylength: 0,
		tileused: [-1,-1,-1,-1,-1,-1,-1],
		
		// functions
        init: function() { // initialize the event handler
			this.bind("KeyDown",this.handlekeys);
			this.bind("GameOver",this.gameover);
			this.bind("Submit",this.submit);
			this.bind("Shuffle",this.shuffle);
			},
		handlekeys: function(ev) {
			// letter - move tile down if valid
			if (ev.keyCode >= Crafty.keys.A && ev.keyCode <= Crafty.keys.Z) {
				var testletter = String.fromCharCode(ev.keyCode);
				// go through letter tiles
				for (i=0;i<this.wordlength;i++) {
					// find tile
					var tile = Crafty("tileno"+i)[0];
					// see if it's used already 
					if (!Crafty(tile).typed) {
						// see if it matches the letter
						if (Crafty(tile).letter == testletter) {
							Crafty(tile).typed = true;
							Crafty(tile).y = 174;
							Crafty(tile).x = Crafty(tile).rackleft+this.entrylength*TileSize;
							this.tileused[this.entrylength] = i;
							this.entrylength++;
							this.entry+=testletter;
							return; // break out of loop
						}
					}
				}
			}
			// enter - check word for accuracy
			if (ev.keyCode == Crafty.keys.ENTER) {
				this.submit();
			}
			// backspace - delete last letter
			if (ev.keyCode == Crafty.keys.BACKSPACE && this.entrylength>0) {
				// find tile
				this.entrylength--;
				var tile = Crafty("tileno"+this.tileused[this.entrylength])[0];
				Crafty(tile).restore();
				this.entry=this.entry.substr(0,this.entrylength);
			}
			// space - shuffle
			if (ev.keyCode == Crafty.keys.SPACE) {
				this.shuffle();
			}
		},
		checkword: function (word) {
			for(var i=0; i<ValidWords.length; i++) {
				if (ValidWords[i] == word) return true;
			}
			return false;
		},
		
		gameover: function() {
			this.unbind("KeyDown");
			//restore letters
			for (i=0;i<this.wordlength;i++) {
				// find tile
				var tile = Crafty("tileno"+i)[0];
				Crafty(tile).restore();
			}
			this.entrylength=0;
			this.entry="";
		},
		
		submit: function() {
			// if this is a valid word, this should grab all the answer letters that are in whatever they typed
			if (this.entry.length < 3) Crafty.trigger("NewMessage","tooshort");
			else {
				var answer = Crafty("word"+this.entry);
				var alreadyfound = false; // will be changed later if it was not found
				if (answer.length>0) { // found something
					// change from empty symbol to appropriate letter for each letter
					for (i=0;i<answer.length;i++) {
						Crafty(answer[i]).addComponent("alpha"+answer.length+Crafty(answer[i]).letter);
						Crafty(answer[i]).removeComponent("word"+this.entry); // take out label so it isn't scored again
						Crafty(answer[i]).removeComponent("marker"+answer.length); // take out marker graphics
					}

					// add this word to the scored words and award score
					ScoredWords.push(this.entry);
					if (this.wordlength == 6) Score += ScoreVal6[this.entry.length-3];
					else Score += ScoreVal7[this.entry.length-3];
					Crafty(Crafty("ScoreText")[0]).text(Score);
					if (ScoredWords.length == ValidWords.length) {
						Crafty.trigger("NewMessage","alldone"); // show all done message
						endRound();
					}
					else Crafty.trigger("NewMessage","wordscored");

				}
				else { // either already found or not there at all
					for (i=0;i<ScoredWords.length;i++) if (ScoredWords[i]==this.entry) {
						Crafty.trigger("NewMessage","alreadyfound");
						alreadyfound = true;
					}
					if (!alreadyfound) Crafty.trigger("NewMessage","notfound");
				}
			}

			//restore letters
			for (i=0;i<this.wordlength;i++) {
				// find tile
				var tile = Crafty("tileno"+i)[0];
				Crafty(tile).restore();
			}
			this.entrylength=0;
			this.entry="";				
		},

		shuffle: function() {
			// randomize and restore all tiles
			var picked=[false,false,false,false,false,false,false];
			for (i=0;i<this.wordlength;i++) {
				// scramble word
				var z;
				do {
					z=Crafty.randRange(0,this.wordlength-1);
				} while (picked[z]==true);
				picked[z]=true;
				// find tile and reposition
				var tile = Crafty("tileno"+i)[0];
				Crafty(tile).position = z;
				Crafty(tile).restore();
			}
			this.entrylength=0;
			this.entry="";
		}
    });

	// -- LOADING ARTWORK -------------------------
	
	// Markers (little squares for unguessed)
	Crafty.sprite(1, "img/Markers.png", { 
		marker3: [0,0,12,12],
		marker4: [12,0,12,12],
		marker5: [24,0,12,12],
		marker6: [36,0,12,12],
		marker7: [48,0,12,12]
	}); 	

	// X and O markers for guessed and unguessed words in challenges
	Crafty.sprite(1, "img/OXmarks.png", { 
		Omark: [0,0,14,14],
		Xmark: [14,0,14,14]
	}); 	

	// Clock elements
	Crafty.sprite(1, "img/ClockHand.png", { 
		clockhand: [0,0,32,72],
	}); 	
	Crafty.sprite(1, "img/ClockCenter.png", { 
		clockcenter: [0,0,21,21],
	}); 	
	Crafty.sprite(1, "img/ClockDecor.png", { 
		clockdecor: [0,0,18,40],
	}); 	
	
	// Messages - text messges for in-game feedback
	Crafty.sprite(1, "img/Messages.png", { 
		notfound: [0,0,200,24],
		tooshort: [0,24,200,24],
		alldone: [0,47,200,24],
		timeup: [0,71,200,24],
		wordscored: [0,94,200,24],
		alreadyfound: [0,117,200,24]
	}); 	

	// Buttons
	Crafty.sprite(1, "img/StartGameOverlay.PNG", { 
		startgame: [0,0,131,85],
	}); 	
	Crafty.sprite(1, "img/SubmitShuffleOverlay.PNG", { 
		submit: [0,0,131,42],
		shuffle: [0,42,131,43],
	}); 	
	Crafty.sprite(1, "img/SendChallengeOverlay.PNG", { 
		submitchallenge: [0,0,131,85],
	}); 	
	Crafty.sprite(1, "img/SeeScores.png", { 
		seescores: [0,0,131,85],
	}); 	
	Crafty.sprite(1, "img/NextRoundOverlay.PNG", { 
		nextround: [0,0,131,85],
	}); 	
	Crafty.sprite(1, "img/SendChallengeOverlay.PNG", { 
		sendchallenge: [0,0,131,85],
	}); 	
	
	// Big letter tiles - easier to build these as arrays
    Crafty.sprite(65, "img/AlphabetTiles65.png", function() {
        var i, spritemap = {};
        for (i = 0; i < 26; i++) {
                spritemap['bigletter' + String.fromCharCode(65+i)] = [i, 0, 1, 1];
            }
        return spritemap;
    }());
	
	// Small alphabets for different word lengths - different colors depending on word length
	for (var j=3;j<=7;j++) {
		Crafty.sprite(12, "img/Alphabet12-"+j+".png", function() {
			var i, spritemap = {};
			for (i = 0; i < 26; i++) {
					spritemap['alpha' + j + String.fromCharCode(65+i)] = [i, 0, 1, 1];
				}
			return spritemap;
		}());
	}

	// Red ones with shaded background - for missed words
	Crafty.sprite(1, "img/Alphabet12Bad.png", function() {
		var i, spritemap = {};
		for (i = 0; i < 26; i++) {
				spritemap['alphabad' + String.fromCharCode(65+i)] = [i*12, 0, 12, 14];
			}
		return spritemap;
		}());


	// -- GAME SETUP FUNCTIONS ---------------------
    // function for setting up the game round

    function setupGame(thisround) {

		// get word list from appropriate clue string
		ValidWords = Clues[thisround].split(" ");

		// reset scored words list
		ScoredWords = []; 
		
		// if we've been challenged, there are opponent results to show
		if (GameType == "challenged") ResultsTags = OpponentContestCode[thisround].split("a");
		else ResultsTags="";
		
		// grab main word and its length - should be last one on list
		var word = ValidWords[ValidWords.length-1];
		var len = ValidWords[ValidWords.length-1].length;
		
		// Now we need to randomize the letters and create the tiles
		var picked=[false,false,false,false,false,false,false];
		for (var i=0;i<len;i++) {
			// scramble word
			var z;
			do {
				z=Crafty.randRange(0,len-1);
			} while (picked[z]==true);
			var theletter = word.charAt(z);
			picked[z]=true;
			// create tile
			Crafty.e("2D, DOM, lettertile, tileno"+i+", bigletter"+theletter)
			.attr({
				tileno: i,
				position: i,
				x: 270-TileSize*0.5*len+TileSize*i,
				y: 88,
				z: 4,
				rackleft: 270-TileSize*0.5*len,
				letter: theletter,
				});

				};

		// setup needed entities
		
		// the keyboard handler runs all the user interface and manages the tiles
		Crafty.e("keyboardhandler, Keyboard")
			.attr({wordlength:len});

		// Message area
		Crafty.e("2D, DOM, messageboard")
			.attr({x:30,y:290});

		// create little words
		// -- first, get the geometry of the grid
		// -- the columns will be 25 words long
		// -- need to figure out the width of the rows (based on the last, biggest word in the column)
		// -- and add spacing between columns
		var rows = 25;
		var cols = Math.round(ValidWords.length/rows);
		if (cols*rows < ValidWords.length) cols++;
		var colwidth = [0,0,0,0,0,0,0];
		var totalwidth=0;
		for (i=0;i<cols;i++) {
			if (i==cols-1) colwidth[i] = ValidWords[ValidWords.length-1].length*12;
			else colwidth[i] = ValidWords[i*rows+(rows-1)].length*12;
			totalwidth += colwidth[i] + 17;
		}

		// knowing width, we can create little letter tiles for each letter in each answer word
		var wx = 700-totalwidth/2;
		for (i=0;i<ValidWords.length;i++) {
			var l = ValidWords[i].length;
			for (j=0;j<l;j++) {
				Crafty.e("2D, DOM, marker"+l+", word"+ValidWords[i])
					.attr({
						x: wx+j*12,
						y: 55+15*(i%rows),
						word: ValidWords[i],
						letter: ValidWords[i].charAt(j)
						});
			}
			// check for contest stuff and add marks
			if (ResultsTags.length>0) {
				var pic;
				if (ResultsTags[i] == 1) pic="Omark";
				else pic = "Xmark";
			
				Crafty.e("2D, DOM, "+pic)
				.attr({
					x: wx-14,
					y: 54+15*(i%rows),
					});
			}
			if (i%rows==rows-1) wx+= colwidth[Math.floor(i/rows)]+17;
		}

		// Score message
		Crafty.e("2D, DOM, Text, ScoreText")
			.attr({
				w: 10,
				h: 20,
				x: 92,
				y: 397
			})
			.text("0")
			.css({"text-align": "center", "color": "#10253f", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "25px"});

		// Opponent Score message
		if (GameType == "challenged") Crafty.e("2D, DOM, Text, OppScore")
			.attr({
				w: 60,
				h: 20,
				x: 162,
				y: 390
			})
			.text(OpponentName+":<br>"+OpponentRoundScore[WhichRound])
			.css({"text-align": "center", "color": "#83A8D5", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "15px"});
	
			
		// Round message
		Crafty.e("2D, DOM, Text, RoundText")
			.attr({
				w: 10,
				h: 20,
				x: 108,
				y: 367
			})
			.text((WhichRound+1).toString())
			.css({"text-align": "center", "color": "#10253f", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "18px"});

			
		var tx=233;
		var ty=283;
		
		// clock parts
		// - central disk - covers up middle of clock hand
		Crafty.e("2D, DOM, clockcenter")
			.attr({
				x: tx+64,
				y: ty+64,
				z: 5
				});

		// - rotating hand - this entity checks to see if the time has run out as well
		var clock=new Date();
		Crafty.e("2D, DOM, clockhand")
			.attr({
				x: tx+75-16,
				y: ty,
				z: 2,
				rotation: 0,
				nextangle: 0,
				startclock: clock.getTime()
				})
			.origin(16,74)
			.bind("EnterFrame",function() {
				var clock=new Date();
				this.rotation = (clock.getTime()-this.startclock) * 360 / (RoundTime*1000);
				if (this.rotation >=this.nextangle) {
					Crafty.e("2D, DOM, clockdecor") // little patterns that fade in
					.attr({
						x: tx+75-9,
						y: ty+75-65,
						z: 1,
						rotation: this.nextangle+22.5,
						alpha:0
						})
					.origin(9,65)
					.bind("EnterFrame",function() {
						var thehand = Crafty("clockhand");
						this.alpha = 0.5*(Crafty(thehand[0]).rotation - this.rotation + 22.5)/45;
						if (this.alpha>0.5) { // stop when halfway transparent
							this.alpha = 0.5;
							this.unbind("EnterFrame");
							}
						});

					this.nextangle+=45;
				}
				if (this.rotation > 360) { // time up
					// stop the clock 
					this.unbind("EnterFrame");
					// mark missed words
					for (var i=3;i<7;i++) {
						var bads = Crafty("marker"+i);
						for (var j=0;j<bads.length;j++) {
							Crafty(bads[j]).addComponent("alphabad"+Crafty(bads[j]).letter);
							Crafty(bads[j]).y--;
							}
						}
					// end round
					Crafty.trigger("NewMessage","timeup");
					endRound();
				}
			})
			.bind("GameOver",function() {		
				this.unbind("EnterFrame");
			});

		// submit button - if they click instead of hitting return
		Crafty.e("2D, DOM, submit, Mouse")
			.attr({
				x: 421,
				y: 262,
				z: 1
				})
			.bind("Click",function() {
				Crafty.trigger("Submit");
			});

		// submit button - if they click instead of hitting space
		Crafty.e("2D, DOM, shuffle, Mouse")
			.attr({
				x: 421,
				y: 304,
				z: 1
				})
			.bind("Click",function() {
				Crafty.trigger("Shuffle");
			});

	} // end setupGame()
	
	function preGame() {

		// set background
		Crafty.background("url('img/WordyBackground.png')");

		// Score message
		Crafty.e("2D, DOM, Text, ScoreText")
			.attr({
				w: 10,
				h: 20,
				x: 92,
				y: 397
			})
			.text("0")
			.css({"text-align": "center", "color": "#10253f", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "25px"});
		
		// Round message 
		Crafty.e("2D, DOM, Text, RoundText")
			.attr({
				w: 10,
				h: 20,
				x: 108,
				y: 367
			})
			.text("1")
			.css({"text-align": "center", "color": "#10253f", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "18px"});

		// Pregame message
		var tw = Rounds+" Round";
		if (Rounds > 1) tw += "s";
		tw += " -- "
		switch (RoundTime) {
			case 30: tw += "30 Seconds"; break;
			case 60: tw += "One Minute"; break;
			case 120: tw += "Two Minutes"; break;
			case 180: tw += "Three Minutes"; break;
			default: tw += "Time error!"; break;
		}
		Crafty.e("2D, DOM, Text, Intro")
			.attr({
				w: 443,
				h: 60,
				x: 50,
				y: 101
			})
			.text(tw)
			.css({"text-align": "center", "color": "#83A8D5", "font-family": "Tahoma,Verdana, sans-serif", "font-size": "35px"});

			
		var tx=233;
		var ty=283;
		
		// clock parts
		Crafty.e("2D, DOM, clockcenter")
			.attr({
				x: tx+64,
				y: ty+64,
				z: 5
				});
		var clock=new Date();
		Crafty.e("2D, DOM, clockhand")
			.attr({
				x: tx+75-16,
				y: ty,
				z: 2,
				rotation: 0,
				nextangle: 0,
				startclock: clock.getTime()
				})
			.origin(16,74);
		Crafty.e("2D, DOM, startgame, Mouse")
			.attr({
				x: 421,
				y: 262,
				z: 1
				})
			.bind("Click",function() {
				Crafty.scene("main");
			});

	} // end preGame
	
	// end of round - get ready for next one or end game
	function endRound() {
		Crafty.trigger("GameOver"); // get clock to stop and disable word entry

		// create contest code 
		var j = 0;
		var scored;
		for (i=0;i<ValidWords.length;i++) {
			scored = false;
			for (j=0;j<ScoredWords.length;j++) {
				if (ScoredWords[j]==ValidWords[i]) scored = true;
			}
			if (scored) MyContestCode[WhichRound] += "1a";
			else MyContestCode[WhichRound] += "0a";
		}
		MyContestCode[WhichRound]=MyContestCode[WhichRound].substr(0,MyContestCode[WhichRound].entrylength); // drop last 'a'

		// record score 
		MyRoundScore[WhichRound] = Score;
		Score = 0;
		
		// advance round and check for end
		WhichRound++;

		if (WhichRound == Rounds) { // we're done
			postScores();
			var art;
			if (GameType == "single") art = "seescores";
			else art = "submitchallenge";
			Crafty.e("2D, DOM, "+art+", Mouse")
				.attr({
					x: 421,
					y: 262,
					z: 1
					})
				.bind("Click",function() {
					window.location = "http://planktongames.com/wordy/scorereport.php";
				});
		}
		else { // next round button
			Crafty.e("2D, DOM, nextround, Mouse")
				.attr({
					x: 421,
					y: 262,
					z: 1
					})
				.bind("Click",function() {
					Crafty.scene("transition");
				});
		}
	} // end of endRound
	
	// end of game - set cookies for score display and send an e-mail to opponent if needed
	function postScores() {

		if (GameType == "single") { // single player game - only need to set up for post-game summary
			MyTotalScore = 0;
			for (var i=0;i<Rounds;i++) {
				setCookie("gamenum"+i,GameNumbers[i]);
				setCookie("clue"+i,Clues[i])
				setCookie("p1score"+i,MyRoundScore[i]);
				setCookie("p1code"+i,MyContestCode[i]);
				if (i<Rounds) MyTotalScore+=MyRoundScore[i];
			}
			setCookie("p1totalscore",MyTotalScore);
		}

		else if (GameType == "mychallenge") { // we challenged somebody else; need to show our scores and send challenge out
			// - we need both to assemble our own report (cookies) and send an e-mail with the form info in a link
			// for the self-reporting, game numbers, names, e-mails, clues should all be set already, 
			// so we just need to set the cookies for the scores
			MyTotalScore = 0;
			for (var i=0;i<Rounds;i++) {
				setCookie("p1score"+i,MyRoundScore[i]);
				setCookie("p1code"+i,MyContestCode[i]);
				if (i<Rounds) MyTotalScore+=MyRoundScore[i];
			}
			setCookie("p1totalscore",MyTotalScore);
			// for the web link, we need to send every piece of this in a GET post, that is, in the URL.
			var emailchallenge = "<html><body><h3>Hi, "+OpponentName+",<br>\nYou have been challenged to a game of Wordy by "+MyName+"!<br>\nClick on the link below to play the challenge!<br>\n<a href=\"http://planktongames.com/wordy/contest.php?";
			emailchallenge += "p1="+escape(MyName)+"&";
			emailchallenge += "p2="+escape(OpponentName)+"&";
			emailchallenge += "e1="+escape(MyEmail)+"&";
			emailchallenge += "e2="+escape(OpponentEmail)+"&";
			emailchallenge += "rounds="+Rounds+"&";
			emailchallenge += "roundtime="+RoundTime+"&";
			emailchallenge += "p1totalscore="+MyTotalScore+"&";
			for (i=0;i<Rounds;i++) { // round information
				emailchallenge += "gn"+i+"="+GameNumbers[i]+"&";
				emailchallenge += "p1s"+i+"="+MyRoundScore[i]+"&";
				emailchallenge += "p1cc"+i+"="+MyContestCode[i]+"&";
			}
			emailchallenge += "seqnum="+SeqNum+"&";
			emailchallenge += "contest=challenge";
			emailchallenge += '">Play Challenge!</a><br>\n<br>\nThanks for playing Wordy!<br>(Game Number '+SeqNum+')</h3></body></html>';
			setCookie("msgtxt",emailchallenge);
		} // end of challenge report

		else if (GameType == "challenged") { // we got challenged; game is now complete 
			// - we need both to assemble our own report (cookies) and send an e-mail with the form info in a link
			// for the self-reporting, game numbers, names, e-mails, clues and opponent scores should all be set already, 
			// so we just need to set the cookies for the scores
			MyTotalScore = 0;
			for (var i=0;i<Rounds;i++) {
				setCookie("p2score"+i,MyRoundScore[i]);
				setCookie("p2code"+i,MyContestCode[i]);
				if (i<Rounds) MyTotalScore+=MyRoundScore[i];
			}
			setCookie("p2totalscore",MyTotalScore);
			// for the web link, we need to send every piece of this in a GET post, that is, in the URL.
			var emailchallenge = "<html><body><h3>Hi, "+OpponentName+",<br>\n"+MyName+" has completed your Wordy challenge!<br>\nClick on the link below to see the results!<br>\n<a href=\"http://planktongames.com/wordy/scorereport.php?";
			emailchallenge += "p2="+escape(MyName)+"&";
			emailchallenge += "p1="+escape(OpponentName)+"&";
			emailchallenge += "e2="+escape(MyEmail)+"&";
			emailchallenge += "e1="+escape(OpponentEmail)+"&";
			emailchallenge += "rounds="+Rounds+"&";
			emailchallenge += "roundtime="+RoundTime+"&";
			emailchallenge += "p2totalscore="+MyTotalScore+"&";
			emailchallenge += "p1totalscore="+OpponentTotalScore+"&";
			for (i=0;i<Rounds;i++) { // round information
				emailchallenge += "gn"+i+"="+GameNumbers[i]+"&";
				emailchallenge += "p2s"+i+"="+MyRoundScore[i]+"&";
				emailchallenge += "p2cc"+i+"="+MyContestCode[i]+"&";
				emailchallenge += "p1s"+i+"="+OpponentRoundScore[i]+"&";
				emailchallenge += "p1cc"+i+"="+OpponentContestCode[i]+"&";
			}
			emailchallenge += "seqnum="+SeqNum+"&";
			emailchallenge += "contest=scorereport";
			emailchallenge += '">See Results!</a><br>\n<br>\nThanks for playing Wordy!<br>(Game Number '+SeqNum+')</h3></body></html>';
			setCookie("msgtxt",emailchallenge);
		} // end of report for getting challenged
	} // end of postScores

	
	//the loading screen that will display while our assets load
	Crafty.scene("loading", function() {
		//load takes an array of assets and a callback when complete
		Crafty.load(["img/AlphabetTiles65.png","img/WordyBackground.png","img/ClockCenter.png","img/ClockHand.png"], function() {
				Crafty.scene("pregame"); //when everything is loaded, run the pregame scene
		});

        //black background with some loading text
		Crafty.background("#770000");
		Crafty.e("2D, DOM, Text")
			.attr({
				w: 100,
				h: 20,
				x: 250,
				y: 120
			})
			.text("Loading")
			.css({"text-align": "center"});
	});

    //automatically play the loading scene
    Crafty.scene("loading");

    Crafty.scene("pregame", function() {
        preGame();
    }); // end main scene

	Crafty.scene("transition", function() {
		// I only shift to this transition scene very briefly to get all the pre-existing entities to die
		Crafty.scene("main");
    }); // end transition scene

    Crafty.scene("main", function() {
        setupGame(WhichRound);
    }); // end main scene
};