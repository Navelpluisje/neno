// Game scene
// -------------
// Runs the core gameplay loop
Crafty.scene( 'Level1', function() {

	Sound.stopAudio();
	Sound.playAudio( 'melody1' );

	Crafty.background( 'url(assets/background.png)' );

	/**
	 * Set the player to the startposition
	 */
	if( Game.lives === 0 ) {
		Game.resetPlayerPosition();
	}
	this.player = Crafty.e('PlayerCharacter').at( parseInt( Game.playerpos.x / 33 ), parseInt( Game.playerpos.y / 33 ) );

	/**
	 * Place a 'Border' at every edge square exept the top ones.
	 */ 
	for (var x = -1; x <= Game.map_grid.width; x++) {
		for (var y = 0; y < Game.map_grid.height; y++) {
			var at_edge = x === -1 || x === Game.map_grid.width || y === Game.map_grid.height - 1;
			if (at_edge) {
				/**
				 * Place a tree entity at the current tile
				 */ 
				Crafty.e('Border').at(x, y);
			}
		}
	}

	/**
	 * Place all our assets in the correct position
	 */
	for( var a = 0; a < assets_level1.length; a++ ) {
		var ax   = assets_level1[a].x,
			ay   = assets_level1[a].y,
			type = assets_level1[a].type;

		/**
		 * If the marble already has been taken, we do not want to lace it in our grid again
		 */
		if( Game.marbles[ ax * 33 ] === ay * 33 ) {
			continue;
		}

		/**
		 * If we already have the key, hide it and show the stair
		 */
		if( Game.grepped_key && type === 'Key' ) {
			Crafty.e( 'Dirt' ).at( 20, 3 );
			continue;
		}


		Crafty.e( type ).at( ax, ay );
	}

	/**
	 * Let the bug crawl
	 */
	Crafty.e( 'Bug' );
	Crafty.e( 'RopeGirl' );

	/**
	 * Show the clock to let the player know how long he's playing
	 */
	var clock = Crafty.e( 'Clock' );
		clock.setTime({ frame: 0, gameTime: Date.now() });
	Crafty.e( 'Time' );
	Crafty.e( 'Score' );
	Crafty.e( 'ScoreBoard' );
	Crafty.e( 'Lives' );


	/**
	 * Play a ringing sound to indicate the start of the journey
	 */
	Sound.playEffect( 'ring' );

	Crafty.trigger( 'SetScore', Game.score );

	// Show the victory screen once all villages are visisted
	this.show_victory = this.bind('PickedMarble', function() {
		Crafty.audio.stop('walking');
		if (!Crafty('Marble').length) {
			Crafty.scene('Victory');
		}
	});
}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
	this.unbind('PickedMarble', this.show_victory);
});