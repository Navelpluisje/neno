// Game scene
// -------------
// Runs the core gameplay loop
Crafty.scene( 'Level2', function() {
	var reserved = [];

	Sound.stopAudio();
	Sound.playAudio( 'melody1' );

	Crafty.background( 'url(assets/background-level2.png)' );

	/**
	 * Place all our assets in the correct position
	 */
	for( var a = 0; a < assets_level2.length; a++ ) {
		var ax   = assets_level2[a].x,
			ay   = assets_level2[a].y,
			type = assets_level2[a].type;

		/**
		 * If the marble already has been taken, we do not want to lace it in our grid again
		 */
		if( Game.marbles[ ax * 33 ] === ay * 33 ) {
			continue;
		}

		/**
		 * If we already have the key, hide it and show the stair
		 */
		if( Game.grepped_key && type === 'Key' ) {
			Crafty.e( 'Dirt' ).at( 20, 3 );
			continue;
		}

		if( ay === Game.map_grid.height - 1 ) {
			reserved[ax] = ax;
		}

		Crafty.e( type ).at( ax, ay );
	}
	/**
	 * Set the player to the startposition
	 * and do some level resetting
	 */
	this.player = Crafty.e('PlayerCharacter').at( parseInt( Game.playerpos.x / 33 ), parseInt( Game.playerpos.y / 33 ) );
	Game.setLevel();
	/**
	 * Place a 'Border' at every edge square exept the top ones.
	 */ 
	for (var x = -1; x <= Game.map_grid.width; x++) {
		for (var y = 0; y < Game.map_grid.height; y++) {
			if( y === Game.map_grid.height - 1 && reserved[x] === x) {
				continue;
			}

			var at_edge = x === -1 || x === Game.map_grid.width || y === Game.map_grid.height - 1;
			if (at_edge) {
				/**
				 * Place a tree entity at the current tile
				 */ 
				Crafty.e('Border').at(x, y);
			}
		}
	}


	/**
	 * Attack of the rope girs
	 */
	girl1 = Crafty.e( 'RopeGirl' );
	girl1._bottom = 9;
	girl1._left   = 8;
	girl1._speed  = 3000;
	girl1.startTween();

	girl2 = Crafty.e( 'RopeGirl' );
	girl2._bottom = 9;
	girl2._left   = 16;
	girl2._speed  = 3500;
	girl2.startTween();

	/**
	 * Show the clock to let the player know how long he's playing
	 */
	Crafty.e( 'Clock' );
	Crafty.e( 'Time' );
	Crafty.e( 'Score' );
	Crafty.e( 'ScoreBoard' );
	Crafty.e( 'Lives' );


	/**
	 * Play a ringing sound to indicate the start of the journey
	 */
	Sound.playAudio( 'ring' );

	Crafty.trigger( 'SetScore', Game.score );

	// Show the victory screen once all villages are visisted
	this.show_victory = this.bind('PickedMarble', function() {
		Crafty.audio.stop('walking');
		if (!Crafty('Marble').length) {
			Crafty.scene('Victory');
		}
	});
}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
	this.unbind('PickedMarble', this.show_victory);
});