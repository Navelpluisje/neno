// Loading scene
// -------------
// Handles the loading of binary assets such as images and audio files
Crafty.scene('Loading', function(){

	Crafty.background( 'url(assets/default-background.png)' );

	// Draw some text for the player to see in case the file
	//  takes a noticeable amount of time to load
	Crafty.e('2D, DOM, Text')
		.text('Loading; please wait...')
		.attr({ x: 0, y: Game.height()/2 - 24, w: Game.width() })
		.textFont($text_css);

	// Load our sprite map image
	Crafty.load([
		'assets/tiles.png',
		'assets/hunter.png',
		'assets/buttons.png',
		'assets/sounds/board_room_applause.mp3',
		'assets/sounds/board_room_applause.ogg',
		'assets/sounds/candy_dish_lid.mp3',
		'assets/sounds/candy_dish_lid.ogg',
		'assets/sounds/key-bleep.mp3',
		'assets/sounds/key-bleep.ogg',
		'assets/sounds/walking.mp3',
		'assets/sounds/walking.ogg',
		'assets/sounds/jump.mp3',
		'assets/sounds/jump.ogg',
		'assets/sounds/ting.mp3',
		'assets/sounds/ting.ogg',
		'assets/sounds/die.mp3',
		'assets/sounds/die.ogg',
		'assets/sounds/headcrunch.mp3',
		'assets/sounds/headcrunch.ogg',
		'assets/sounds/watersplash.mp3',
		'assets/sounds/watersplash.ogg',
		'assets/sounds/melody1.mp3',
		'assets/sounds/melody1.ogg',
		'assets/sounds/melody2.mp3',
		'assets/sounds/melody2.ogg',
		], function(){
		// Once the images are loaded...

		// Define the individual sprites in the image
		// Each one (spr_tree, etc.) becomes a component
		// These components' names are prefixed with "spr_"
		//  to remind us that they simply cause the entity
		//  to be drawn with a certain sprite
		Crafty.sprite( 'assets/tiles.png', {
		//  Assetname    [  x,   y,   w,   h]
			spr_chest:   [  0,   0,  33,  33],
			spr_dirt:    [ 33,   0,  33,  33],
			spr_rock:    [ 66,   0,  33,  33],
			spr_marble:  [  0,  33,  33,  33],
			spr_empty:   [ 33,  33,  33,  33],
			spr_door:    [ 66,  33,  33,  33],
			spr_key:     [ 10,  66,  16,  33],
			spr_rbug:    [ 33,  66,  33,  33],
			spr_lbug:    [ 66,  66,  33,  33],
			spr_movable: [  0,  99,  33,  33],
			spr_ropeup:  [ 33,  99,  33,  33],
			spr_ropedwn: [ 66,  99,  33,  33],
			spr_bush:    [ 99,   0,  66,  66],
			spr_water:   [165,   0,  33,  33],
			spr_live1:   [  0, 132,  77,  20],
			spr_live2:   [  0, 152,  77,  20],
			spr_live3:   [  0, 172,  77,  20],
			spr_live1k:  [  0, 192,  77,  20],
			spr_live2k:  [  0, 212,  77,  20],
			spr_live3k:  [  0, 232,  77,  20],
			spr_score:   [ 77, 132,  71,  22],
			spr_time:    [ 77, 154,  56,  22],
			btn_save:    [132, 176,  56,  22],
			btn_save_act:[ 77, 198,  56,  22],
			spr_bonus:   [ 77, 220,  72,  22],
			// Some numbers
			spr_nb1:     [107, 176,  15,  22],
			spr_nb2:     [ 92, 176,  15,  22],
			spr_nb3:     [ 77, 176,  15,  22],
			spr_nb4:     [179, 132,  15,  22],
			spr_nb5:     [164, 132,  15,  22],
			spr_nb6:     [149, 132,  15,  22],
			spr_nb7:     [179, 154,  15,  22],
			spr_nb8:     [164, 154,  15,  22],
			spr_nb9:     [134, 154,  15,  22],
			spr_nb0:     [149, 154,  15,  22],
			spr_colon:   [120, 176,  10,  22]
		});

		// Define the PC's sprite to be the first sprite in the third row of the
		//  animation sprite map
		Crafty.sprite(33, 'assets/hunter.png', {
			spr_player:  [0, 0],
		}, 0, 1);

		Crafty.sprite( 'assets/buttons.png', {
		//  Butttonname       [ x,   y,   w,  h ]
			btn_start:        [ 0,   0, 125, 35 ],
			btn_start_act:    [ 0,  35, 125, 35 ],
			btn_settings:     [ 0,  70, 181, 35 ],
			btn_settings_act: [ 0, 105, 181, 35 ],
		} );

		// Define our sounds for later use
		Crafty.audio.add({
			applause:    [ 'assets/sounds/board_room_applause.mp3', 'assets/sounds/board_room_applause.ogg' ],
			ring:        [ 'assets/sounds/candy_dish_lid.mp3', 'assets/sounds/candy_dish_lid.ogg' ],
			key_bleep:   [ 'assets/sounds/key-bleep.mp3', 'assets/sounds/key-bleep.ogg' ],
			walking:     [ 'assets/sounds/walking.mp3', 'assets/sounds/walking.ogg' ],
			jump:        [ 'assets/sounds/jump.mp3', 'assets/sounds/jump.ogg' ],
			ting:        [ 'assets/sounds/ting.mp3', 'assets/sounds/ting.ogg' ],
			die:         [ 'assets/sounds/die.mp3', 'assets/sounds/die.ogg' ],
			headcrunch:  [ 'assets/sounds/headcrunch.mp3', 'assets/sounds/headcrunch.ogg' ],
			watersplash: [ 'assets/sounds/watersplash.mp3', 'assets/sounds/watersplash.ogg' ],
			melody1:     [ 'assets/sounds/melody1.mp3', 'assets/sounds/melody1.ogg' ],
			melody2:     [ 'assets/sounds/melody2.mp3', 'assets/sounds/melody2.ogg' ],
		});

		// Now that our sprites are ready to draw, start the game
		Crafty.scene('Start');
	});
});