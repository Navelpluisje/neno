
/**
 * Question scene
 */
Crafty.scene( 'Question_x', function() {
	// Display some text in celebration of the victory
	Crafty.e('2D, DOM, Text')
		.text('Question')
		.attr({ x: 0, y: Game.height()/2 - 24, w: Game.width() })
		.textFont($text_css);

	// Give'em a round of applause!
	// Crafty.audio.play('applause');

	// After a short delay, watch for the player to press a key, then restart
	// the game when a key is pressed
	this.restart_game = function() {
			Crafty.scene( 'Game' );
	};
	Crafty.bind( 'KeyDown' , this.restart_game );
}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
	this.unbind('KeyDown', this.restart_game);
} );
