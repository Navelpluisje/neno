/*jshint multistr: true */
/**
 * Startscreen for the game. 
 * It will show you a nice and friendly welcome with some settings.
 */
Crafty.scene('Settings', function() {

	Sound.stopAudio();

	/**
	 * Set the background
	 */
	Crafty.background( 'url(assets/default-background.png)' );

	/**
	 * create the html form for changing your settings
	 */
	Crafty.e( '2D, DOM, HTML' )
		.attr( { x: 380, y: 50, w: 400 } )
		.append('\
			<div class="settings"> \
				<p>\
					<label for="audio">Audio</label>\
					<input type="checkbox" name="audio" id="audio" value="1" ' + (Sound.settings.audio == 1 ? 'checked="checked"' : '') + '/> \
				</p>\
				<p>\
					<label for="audio-vol">Audio volume</label>\
					<input type="number" name="audio-vol" id="audio-vol" min="0" max="1" step="0.1" value="' + Sound.settings.audio_vol + '" />\
				</p>\
				<p>\
					<label for="effects">Effects</label>\
					<input type="checkbox" name="effects" id="effects" value="1" ' + (Sound.settings.effects == 1 ? 'checked="checked"' : '') + '/> \
				</p>\
				<p>\
					<label for="effect-vol">Effects volume</label>\
					<input type="number" name="effect-vol" id="effects-vol" min="0" max="1" step="0.1" value="' + Sound.settings.effects_vol + '" />\
				</p>\
			</div>\
			');

	/**
	 * Add the startbutton
	 */
	Crafty.e( 'SettingsButton' )
		.attr( {x: 400, y: 70} );


	/**
	 * Add the startbutton
	 */
	Crafty.e( 'SaveButton' )
		.attr( {x: 400, y: 340} );

	/**
	 * Add the startbutton
	 */
	Crafty.e( 'Button' )
		.attr( {x: 48, y:344} );

	this.save_settings = this.bind( 'SaveSettings', function() {
		Sound.setValue( 'audio', document.getElementById('audio').checked ? 1 : 0 );
		Sound.setValue( 'audio_vol', parseFloat( document.getElementById('audio-vol').value ) );

		Sound.setValue( 'effects', document.getElementById('effects').checked ? 1 : 0 );
		Sound.setValue( 'effects_vol', parseFloat( document.getElementById('effects-vol').value ) );

	} );

}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
});

