/**
 * Startscreen for the game. 
 * It will show you a nice and friendly welcome with some settings.
 */
Crafty.scene('Start', function() {

	Sound.stopAudio();
	Sound.playAudio( 'melody2' );

	/**
	 * Set the background
	 */
	Crafty.background( 'url(assets/default-background.png)' );

	/**
	 * Add the startbutton
	 */
	Crafty.e( 'Button' )
		.attr( {x: 48, y:344} );

	/**
	 * Add the settingsbutton
	 */
	Crafty.e( 'SettingsButton' )
		.attr( {x: 48, y:394} );

}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
});

