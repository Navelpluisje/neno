// Victory scene
// -------------
// Tells the player when they've won and lets them start a new game
Crafty.scene('Victory', function() {

	Sound.stopAudio();
	Sound.playAudio( 'melody2' );

	Crafty.background( 'url(assets/default-background.png)' );

	/**
	 * Managed to go to the victory screen. 
	 * Let's increment the level
	 */

	// Display some text in celebration of the victory
	Crafty.e( 'Woohoo' );
	Crafty.e( 'LevelScore' );
	Crafty.e( 'Score' )
		.attr( { x:45, y:390 } );
	Crafty.e( 'Colon' )
		.attr( { x:120, y:390 } );

	Crafty.e( '2D, DOM, Text' )
		.text( 'You just finished level ' + Game.level)
		.attr( { x: Game.width()/2, y: 6 * 33, w:Game.width()/2 - 66 } )
		.textColor( '#333333' )
		.textFont( {
			'size': '20px', 
			'weight': 'bold',
			'family': 'Verdana', 
			'textAlign': 'left'
		} );

	Game.level += 1;

	var message = 'You\'ve successfully finished the game';
	if( Game.checkLevel( Game.level ) ) {
		message = 'Click the button to start level ' + Game.level;
		Crafty.e( 'Button' )
			.attr( { x: Game.width()/2 + 90, y: 10 * 33 } );
	}
	
	Crafty.e( '2D, DOM, Text' )
		.text( message )
		.attr( { x: Game.width()/2, y: 7 * 33, w: Game.width()/2 - 66 } )
		.textColor( '#333333' )
		.textFont( {
			'size': '20px', 
			'weight': 'bold',
			'family': 'Verdana', 
			'text-align': 'left'
		} );
	this.delay = true;
	/**
	 * Will be triggered when the bonus is set and the score needs to be put in the screen
	 * After that a nice applause will be heard
	 */
	this.bind( 'PutLevelScore', function() {
		this.delay = false;
		var sb = Crafty.e( 'ScoreBoard' );
			sb._ypos = 390;
			sb._xpos = 145;
			sb.setScore( Game.score );
		Crafty.audio.play('applause');

	} );


	// After a short delay, watch for the player to press a key, then restart
	// the game when a key is pressed
	this.play_level = function() {
		if (!this.delay) {
			Crafty.scene( Game.levels[ Game.level ] );
		}
	};
	if( Game.checkLevel( Game.level ) ) {
		Crafty.bind( 'KeyDown', this.play_level );
	}
}, function() {
	// Remove our event binding from above so that we don't
	//  end up having multiple redundant event watchers after
	//  multiple restarts of the game
	this.unbind('KeyDown', this.play_level);
});
